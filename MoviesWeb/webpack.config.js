var path = require("path");
var webpack = require('webpack');

module.exports = {
    entry: './scripts/index.jsx',
    output: {
        path: path.resolve(__dirname, 'wwwroot'),
        filename: 'bundle.js'
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['babel'],
            }
        ]
    },

    devtool: 'source-map',

    devServer: {
        inline: true,
        contentBase: 'wwwroot/'
    }
};