﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesWeb.Models;
using MoviesWeb.Views;
using System.Threading.Tasks;

namespace MoviesWeb.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        private readonly AppDbContext dbContext;

        public MoviesController()
        {
            dbContext = new AppDbContext();
        }

        [HttpGet]
        public IActionResult Get()
        {
            var movies = dbContext.Movies.Include(m => m.MovieGenres).ThenInclude(g => g.Genre).Include(m => m.MovieActors).ThenInclude(g => g.Actor).ToList();
            if (!movies.Any()) return NotFound();
            var movieViews = new List<MovieView>();
            for (var i=0;i<movies.Count(); i++)
            {
                var tempmov = new MovieView
                {
                    Name = movies[i].Name,
                    ReleaseDate = movies[i].ReleaseDate,
                    MovieId = movies[i].MovieId
                };
                foreach (var genre in movies[i].MovieGenres)
                {
                    tempmov.Genres.Add(genre.Genre.Name);
                }
                foreach (var actor in movies[i].MovieActors)
                {
                    tempmov.Actors.Add(actor.Actor.FullName);
                }
                movieViews.Add(tempmov);
            }
            return Ok(movieViews);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var movies = dbContext.Movies.Include(m => m.MovieGenres).ThenInclude(g => g.Genre).Include(m => m.MovieActors).ThenInclude(g => g.Actor).FirstOrDefault(z => z.MovieId == id);
            if (movies == null) return NotFound();
            var mov = new MovieView
            {
                Name = movies.Name,
                ReleaseDate = movies.ReleaseDate,
                MovieId = movies.MovieId
            };
            foreach (var genre in movies.MovieGenres)
            {
                mov.Genres.Add(genre.Genre.Name);
            }
            foreach (var actor in movies.MovieActors)
            {
                mov.Actors.Add(actor.Actor.FullName);
            }
            return Ok(mov);
        }

        [HttpGet("Filtering")]
        public IActionResult Filtering(string movieNameFragment, string genre, string releaseDate)
        {
            var movies = dbContext.Movies.Include(m => m.MovieGenres)
                .ThenInclude(g => g.Genre)
                .Select(m => new MovieView()
                {
                    MovieId = m.MovieId,
                    Name = m.Name,
                    ReleaseDate = m.ReleaseDate,
                    Genres = m.MovieGenres.Select(g => g.Genre.Name.ToString()).ToList(),
                    Actors = m.MovieActors.Select(g => g.Actor.FullName.ToString()).ToList()
                })
                ;
            if (!string.IsNullOrEmpty(movieNameFragment))
            {
                movies = movies.Where(m => m.Name.Contains(movieNameFragment));
            }
            if (!string.IsNullOrEmpty(genre))
            {
                movies = movies.Where(m => m.Genres.Contains(genre));
            }
            if (!string.IsNullOrEmpty(releaseDate))
            {
                movies = movies.Where(m => m.ReleaseDate == DateTime.Parse(releaseDate));
            }
            return Ok(movies);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] MovieView movie)
        {
            var itemToUpdate = dbContext.Movies.FirstOrDefault(z => z.MovieId == id);
            if (itemToUpdate == null)
            {
                return Ok("Movie can't be found in the database.");
            }
            if (!movie.Actors.All(a => dbContext.Actors.Any(actor => actor.FullName == a)))
            {
                return Ok("Some of the actors are invalid. Please input them to the actors database first.");
            }
            if (!movie.Genres.All(a => dbContext.Genres.Any(genre => genre.Name == a)))
            {
                return Ok("Some of the genres are invalid. Please input them to the genres database first.");
            }
            if (!movie.Actors.Any())
            {
                return Ok("Please input at least one actor.");
            }
            if (!movie.Genres.Any())
            {
                return Ok("Please input at least one genre.");
            }

            foreach (var mg in dbContext.MovieGenres)
            {
                if (mg.MovieId == id)
                {
                    dbContext.MovieGenres.Remove(mg);
                }
            }
            dbContext.SaveChanges();
            foreach (var genre in movie.Genres)
            {
                dbContext.MovieGenres.Add(new MovieGenre()
                {
                    GenreId = dbContext.Genres.First(g => g.Name == genre).GenreId,
                    MovieId = id
                });
            }
            dbContext.SaveChanges();
            foreach (var ma in dbContext.MovieActors)
            {
                if (ma.MovieId == id)
                {
                    dbContext.MovieActors.Remove(ma);
                }
            }
            dbContext.SaveChanges();
            foreach (var actor in movie.Actors)
            {
                dbContext.MovieActors.Add(new MovieActor()
                {
                    ActorId = dbContext.Actors.First(g => g.FullName == actor).ActorId,
                    MovieId = id
                });
            }

            itemToUpdate.Name = movie.Name;
            itemToUpdate.ReleaseDate = movie.ReleaseDate;
            dbContext.SaveChanges();
            return Ok("Item successfully updated.");
        }

        [HttpPost]
        public IActionResult Post([FromBody]MovieView movie)
        {
            if (movie==null)
            {
                return Ok("Please input data.");
            }
            if (dbContext.Movies.Any(m => m.Name == movie.Name))
            {
                return Ok("Movie with this name already exists.");
            }
            var itemToAdd = new Movie();
            if (movie.Actors == null || !movie.Actors.Any())
            {
                return Ok("Please input at least one actor.");
            }
            if (movie.Genres == null || !movie.Genres.Any())
            {
                return Ok("Please input at least one genre.");
            }
            if (!movie.Actors.All(a => dbContext.Actors.Any(actor => actor.FullName == a)))
            {
                return Ok("Some of the actors are invalid. Please input them to the actors database first.");
            }
            if (!movie.Genres.All(a => dbContext.Genres.Any(genre => genre.Name == a)))
            {
                return Ok("Some of the genres are invalid. Please input them to the genres database first.");
            }

            itemToAdd.Name = movie.Name;
            itemToAdd.ReleaseDate = movie.ReleaseDate;
            dbContext.Movies.Add(itemToAdd);
            dbContext.SaveChanges();
            var addedMovie = dbContext.Movies.First(m => m.Name == movie.Name);
            foreach (var genre in movie.Genres)
            {
                dbContext.MovieGenres.Add(new MovieGenre()
                {
                    GenreId = dbContext.Genres.First(g => g.Name == genre).GenreId,
                    MovieId = addedMovie.MovieId
                });
            }
            dbContext.SaveChanges();
            foreach (var actor in movie.Actors)
            {
                dbContext.MovieActors.Add(new MovieActor()
                {
                    ActorId = dbContext.Actors.First(g => g.FullName == actor).ActorId,
                    MovieId = addedMovie.MovieId
                });
            }
            dbContext.SaveChanges();
            return Ok("Item successfully added.");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var mov = dbContext.Movies.FirstOrDefault(z => z.MovieId == id);
            if (mov == null)
            {
                return NotFound();
            }
            dbContext.Movies.Remove(mov);
            dbContext.SaveChanges();
            return Ok("Movie successfully deleted.");
        }
    }
}
