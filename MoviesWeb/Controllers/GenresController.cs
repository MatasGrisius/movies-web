﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesWeb.Models;
using MoviesWeb.Views;

namespace MoviesWeb.Controllers
{
    [Route("api/[controller]")]
    public class GenresController : Controller
    {
        private readonly AppDbContext dbContext;

        public GenresController()
        {
            dbContext = new AppDbContext();
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(dbContext.Genres);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var genre = dbContext.Genres.FirstOrDefault(g => g.GenreId == id);
            if (genre != null)
            {
                return Ok(genre);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Post([FromBody]Genre genre)
        {
            if (dbContext.Genres.Any(g => g.Name == genre.Name))
            {
                return Ok("Genre already exists in the database.");
            }
            dbContext.Genres.Add(genre);
            dbContext.SaveChanges();
            return Ok("New movie genre successfully added.");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var genre = dbContext.Genres.FirstOrDefault(z => z.GenreId == id);
            if (genre == null)
            {
                return NotFound();
            }
            dbContext.Genres.Remove(genre);
            dbContext.SaveChanges();
            return Ok("Movie genre successfully deleted.");
        }
    }
}
