﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesWeb.Models;
using MoviesWeb.Views;

namespace MoviesWeb.Controllers
{
    [Route("api/[controller]")]
    public class ActorsController : Controller
    {
        private readonly AppDbContext dbContext;

        public ActorsController()
        {
            dbContext = new AppDbContext();
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(dbContext.Actors);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var actor = dbContext.Actors.FirstOrDefault(g => g.ActorId == id);
            if (actor != null)
            {
                return Ok(actor);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Post([FromBody]Actor actor)
        {
            if (dbContext.Actors.Any(a => a.FullName == actor.FullName))
            {
                return Ok("Genre already exists in the database.");
            }
            dbContext.Actors.Add(actor);
            dbContext.SaveChanges();
            return Ok("New actor successfully added.");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var actor = dbContext.Actors.FirstOrDefault(z => z.ActorId == id);
            if (actor == null)
            {
                return NotFound();
            }
            dbContext.Actors.Remove(actor);
            dbContext.SaveChanges();
            return Ok("Movie actor successfully deleted.");
        }
    }
}
