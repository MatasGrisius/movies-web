﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MoviesWeb.Migrations
{
    public partial class ChangedActorData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Actors");

            migrationBuilder.RenameColumn(
                name: "Surname",
                table: "Actors",
                newName: "FullName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "Actors",
                newName: "Surname");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Actors",
                nullable: true);
        }
    }
}
