import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Switch, Route, Link} from 'react-router-dom';
import axios from 'axios';

var dateFormat = require('dateformat');


const MoviesList = () => (
  <Switch>
    <Route exact path='/movieslist' component={MoviesContainer}/>
    <Route path='/movieslist/:number' component={MovieDetails}/>
  </Switch>
)

const GenresList = () => (
  <Switch>
    <Route exact path='/genreslist' component={GenresContainer}/>
    <Route path='/genreslist/:number' component={GenreDetails}/>
  </Switch>
)

const ActorsList = () => (
  <Switch>
    <Route exact path='/actorslist' component={ActorsContainer}/>
    <Route path='/actorslist/:number' component={ActorDetails}/>
  </Switch>
)


class NewMovie extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        newmoviename:"",
        newmoviereleasedate:"",
        newmovieactors:[],
        newmoviegenres:[]
      };

      this.setStatebound = this.setState.bind(this);
      this.handleButtonPress = this.handleButtonPress.bind(this);
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleGenresChange = this.handleGenresChange.bind(this);
      this.handleReleaseDateChange = this.handleReleaseDateChange.bind(this);
      this.handleActorsChange = this.handleActorsChange.bind(this);
    }

   handleNameChange(event) {
     this.setStatebound(
       {newmoviename: event.target.value}
     );
   }

      handleReleaseDateChange(event) {
     this.setStatebound(
       {newmoviereleasedate: event.target.value}
     );
   }


   handleGenresChange(event) {
     this.setStatebound(
       {newmoviegenres: event.target.value.split("\n")}
     );
   }


   handleActorsChange(event) {
     this.setStatebound(
       {newmovieactors: event.target.value.split("\n")}
     );
   }


   handleButtonPress() {
      axios.post(`api/movies`,
          {
            "name": this.state.newmoviename,
            "releaseDate": this.state.newmoviereleasedate,
            "genres": this.state.newmoviegenres,
            "actors": this.state.newmovieactors
        }
      )
   .then(function (response) {
      window.alert(response.data);
    });
   }

    render() {
        return (
            <div>
                <h1>Add a new movie</h1><br/>
                Movie name<br />
                <input 
                  type="text" 
                  name="MovieNameTextInput" 
                  onChange={this.handleNameChange}/>
                <br/>


                Genres (new line per genre)
                <br />
                <textarea
                  name="GenreTextArea" 
                  cols="40" 
                  rows="3"
                  onChange={this.handleGenresChange}>
                </textarea>
                <br/>


                Actors (new line per actor)
                <br />
                <textarea
                  name="ActorTextArea" 
                  cols="40" 
                  rows="3"
                  onChange={this.handleActorsChange}>
                </textarea>
                <br/>


                Release date (YYYY-MM-DD) 
                <br />
                <input 
                  type="text" 
                  name="ReleaseDateTextInput" 
                  onChange={this.handleReleaseDateChange}/>
                <br/>


                <button 
                  type="button"
                  onClick={this.handleButtonPress}>Submit a new movie!</button>
                <br/>
            </div>
        )
    }
}

class MoviesContainer extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        movies: [],
        genresearchbox:"",
        moviefragmentsearchbox:"",
        releasedatesearchbox:"",
      };

      this.handleMovieFragmentSearchBox = this.handleMovieFragmentSearchBox.bind(this);
      this.handleGenreSearchBox = this.handleGenreSearchBox.bind(this);
      this.handleReleaseDateSearchBox = this.handleReleaseDateSearchBox.bind(this);
      this.handleSearchButtonPress = this.handleSearchButtonPress.bind(this);
    }

    handleMovieFragmentSearchBox(event) {
     this.setState(
       {moviefragmentsearchbox: event.target.value}
     );
   }

  handleGenreSearchBox(event) {
     this.setState(
       {genresearchbox: event.target.value}
     );
   }

  handleReleaseDateSearchBox(event) {
     this.setState(
       {releasedatesearchbox: event.target.value}
     );
   }

   handleSearchButtonPress() {
      axios.get(`api/movies/filtering?movieNameFragment=${this.state.moviefragmentsearchbox}&genre=${this.state.genresearchbox}&releaseDate=${this.state.releasedatesearchbox}`
      )
      .then(res => {
        const movies = res.data;
        this.setState({ movies });
      });
   }

    componentDidMount() {
    axios.get(`api/movies`)
      .then(res => {
        const movies = res.data;
        this.setState({ movies });
      });
  }

    render() {
        return (
            <div>
              <h3>Search options:</h3>
              Input a fragment of a movie name
                <input 
                  type="text" 
                  name="MovieFragmentSearchBox" 
                  onChange={this.handleMovieFragmentSearchBox}/>
                  
                  <br/>
                  Input an EXACT genre name
                <input 
                  type="text" 
                  name="GenreSearchBox" 
                  onChange={this.handleGenreSearchBox}/>
                  <br/>
                  Input an EXACT movie release date (YYYY-MM-DD)
                <input 
                  type="text" 
                  name="ReleaseDateSearchBox" 
                  onChange={this.handleReleaseDateSearchBox}/>
                  <br/>
                  
                                  <button 
                  type="button"
                  onClick={this.handleSearchButtonPress}>Search!</button>
                <br/>

                <h1>Movies list:</h1>
                <hr/>

            <table>
              <tbody><tr>
                  <th>Name</th>
                  <th>Release Date</th>
                  <th>Genre</th>
                  <th>Actors</th>
                </tr>

                {this.state.movies.map(m => 
                <tr key={m.movieId}>
                  <td><Link to={`/movieslist/${m.movieId}`}>{m.name}</Link></td>
                  <td>{dateFormat(m.releaseDate, "yyyy")}</td>
                  <td>{m.genres.join(", ")}</td>
                  <td>{m.actors.join(", ")}</td>
              </tr>
              )}
              </tbody></table>
            </div>
        )
    }
}

class MovieDetails extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        movie:{name:""},
        found:false,
        newmoviename:"",
        newmoviereleasedate:"",
        newmovieactors:[],
        newmoviegenres:[]
      };

      this.setStatebound = this.setState.bind(this);
      this.handleUpdateButtonPress = this.handleUpdateButtonPress.bind(this);
      this.handleDeleteButtonPress = this.handleDeleteButtonPress.bind(this);
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleGenresChange = this.handleGenresChange.bind(this);
      this.handleReleaseDateChange = this.handleReleaseDateChange.bind(this);
      this.handleActorsChange = this.handleActorsChange.bind(this);
    }

    componentDidMount() {
      const movieId = parseInt(this.props.match.params.number, 10);
      console.log(movieId);
    axios.get(`api/movies/${movieId}`)
      .catch(function (error) {
        console.log("Movie not found");
        this.setStatebound({
          found: false,
        })
      })
      .then(res => {
        console.log(res.data);
        const movie = res.data;
        this.setStatebound({ movie });
        this.setStatebound({ newmoviename: movie.name });
        this.setStatebound({ newmoviereleasedate: movie.releaseDate });
        this.setStatebound({ newmovieactors: movie.actors });
        this.setStatebound({ newmoviegenres: movie.genres });
        this.setStatebound({
          found: true,
        })
      });
   }

   handleNameChange(event) {
     this.setStatebound(
       {newmoviename: event.target.value}
     );
   }

      handleReleaseDateChange(event) {
     this.setStatebound(
       {newmoviereleasedate: event.target.value}
     );
   }


   handleGenresChange(event) {
     this.setStatebound(
       {newmoviegenres: event.target.value.split("\n")}
     );
   }


   handleActorsChange(event) {
     this.setStatebound(
       {newmovieactors: event.target.value.split("\n")}
     );
   }

   handleDeleteButtonPress() {
      axios.delete(`api/movies/${this.state.movie.movieId}`
      )
   .then(function (response) {
      window.alert(response.data);
    })};


   handleUpdateButtonPress() {
      axios.put(`api/movies/${this.state.movie.movieId}`,
          {
            "name": this.state.newmoviename,
            "releaseDate": this.state.newmoviereleasedate,
            "genres": this.state.newmoviegenres,
            "actors": this.state.newmovieactors
        }
      )
   .then(function (response) {
      window.alert(response.data);
    });
   }

    render() {
        if (!this.state.found) {
            return <div>Sorry, but the movie was not found</div>
        }
        return (
            <div>
                <h1>{this.state.movie.name} ({dateFormat(this.state.movie.releaseDate,"yyyy")}) description:</h1><br/>
                Movie name<br />
                <input 
                  type="text" 
                  name="MovieNameTextInput" 
                  defaultValue={this.state.movie.name}
                  onChange={this.handleNameChange}/>
                <br/>


                Genres (new line per genre)
                <br />
                <textarea
                  name="GenreTextArea" 
                  cols="40" 
                  rows="3"
                  defaultValue={this.state.movie.genres.join("\n")}
                  onChange={this.handleGenresChange}>
                </textarea>
                <br/>


                Actors (new line per actor)
                <br />
                <textarea
                  name="ActorTextArea" 
                  cols="40" 
                  rows="3"
                  defaultValue={this.state.movie.actors.join("\n")}
                  onChange={this.handleActorsChange}>
                </textarea>
                <br/>


                Release date (YYYY-MM-DD) 
                <br />
                <input 
                  type="text" 
                  name="ReleaseDateTextInput" 
                  defaultValue={dateFormat(this.state.movie.releaseDate,"yyyy-mm-dd")}
                  onChange={this.handleReleaseDateChange}/>
                <br/>


                <button 
                  type="button"
                  onClick={this.handleUpdateButtonPress}>Update description!</button>
                <br/>

                <button 
                  type="button"
                  onClick={this.handleDeleteButtonPress}>Delete this movie!</button>
                <br/>


                <Link 
                  to='/movieslist'>
                    <button 
                        type="button">Back
                    </button>
                </Link>
            </div>
        )
    }
}

class GenresContainer extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        genres: [],
        newgenre:""
      };

      this.handleTextBoxEdit = this.handleTextBoxEdit.bind(this);
      this.handleButtonPress = this.handleButtonPress.bind(this);
    }

    componentDidMount() {
    axios.get(`api/genres`)
      .then(res => {
        this.setState({ genres: res.data });
      });
  }

  handleTextBoxEdit(event) {
      this.setState({newgenre: event.target.value})
  }

  handleButtonPress() {
      axios.post(`api/genres`,{
        "name":this.state.newgenre
      }).then(res => {
        this.componentDidMount();
        window.alert(res.data);
      });
  }

    render() {
        return (
            <div>
              <h2>Add new genre to the database:</h2>
                <input 
                  type="text" 
                  name="NewGenreTextInput" 
                  onChange={this.handleTextBoxEdit}/>
                  
                  <button 
                    type="button"
                    onClick={this.handleButtonPress}>Submit!</button>
                  <br/>
              <hr/>
              <h2>Possible movie genres list:</h2>
              {this.state.genres.map(m => 
                <li key={m.genreId} ><Link to={`/genreslist/${m.genreId}`}>{m.name}</Link></li>
              )}
            </div>
        )
    }
}

class GenreDetails extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        found:false,
      };

      this.setStatebound = this.setState.bind(this);
      this.handleDeleteButtonPress = this.handleDeleteButtonPress.bind(this);
    }

    componentDidMount() {
      const genreId = parseInt(this.props.match.params.number, 10);
    axios.get(`api/genres/${genreId}`)
      .catch(function (error) {
        this.setStatebound({
          found: false,
        })
      })
      .then(res => {
        this.setStatebound({ genre: res.data });
        this.setStatebound({
          found: true,
        })
      });
   }

   handleDeleteButtonPress() {
      axios.delete(`api/genres/${this.state.genre.genreId}`
      )
   .then(function (response) {
      window.alert(response.data);
    })};

    render() {
        if (!this.state.found) {
            return <div>Sorry, but the genre was not found</div>
        }
        return (
            <div>
                <h1>{this.state.genre.name}</h1><br/>

                <button 
                  type="button"
                  onClick={this.handleDeleteButtonPress}>Delete this genre!</button>
                <br/>


                <Link 
                  to='/genreslist'>
                    <button 
                        type="button">Back
                    </button>
                </Link>
            </div>
        )
    }
}

class ActorsContainer extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        actors: [],
        newactor:""
      };

      this.handleTextBoxEdit = this.handleTextBoxEdit.bind(this);
      this.handleButtonPress = this.handleButtonPress.bind(this);
    }

    componentDidMount() {
    axios.get(`api/actors`)
      .then(res => {
        this.setState({ actors: res.data });
      });
  }

  handleTextBoxEdit(event) {
      this.setState({newactor: event.target.value})
  }

  handleButtonPress() {
      axios.post(`api/actors`,{
        "fullName":this.state.newactor
      }).then(res => {
        this.componentDidMount();
        window.alert(res.data);
      });
  }

    render() {
        return (
            <div>
              <h2>Add new actor to the database:</h2>
                <input 
                  type="text" 
                  name="NewActorTextInput" 
                  onChange={this.handleTextBoxEdit}/>
                  
                  <button 
                    type="button"
                    onClick={this.handleButtonPress}>Submit!</button>
                  <br/>
              <hr/>
              <h2>Possible movie actors list:</h2>
              {this.state.actors.map(m => 
                <li key={m.actorId} ><Link to={`/actorslist/${m.actorId}`}>{m.fullName}</Link></li>
              )}
            </div>
        )
    }
}

class ActorDetails extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        found:false,
      };

      this.setStatebound = this.setState.bind(this);
      this.handleDeleteButtonPress = this.handleDeleteButtonPress.bind(this);
    }

    componentDidMount() {
      const actorId = parseInt(this.props.match.params.number, 10);
    axios.get(`api/actors/${actorId}`)
      .catch(function (error) {
        this.setStatebound({
          found: false,
        })
      })
      .then(res => {
        this.setStatebound({ actor: res.data });
        this.setStatebound({
          found: true,
        })
      });
   }

   handleDeleteButtonPress() {
      axios.delete(`api/actors/${this.state.actor.actorId}`
      )
   .then(function (response) {
      window.alert(response.data);
    })};

    render() {
        if (!this.state.found) {
            return <div>Sorry, but the actor was not found</div>
        }
        return (
            <div>
                <h1>{this.state.actor.fullName}</h1><br/>

                <button 
                  type="button"
                  onClick={this.handleDeleteButtonPress}>Delete this actor!</button>
                <br/>


                <Link 
                  to='/actorslist'>
                    <button 
                        type="button">Back
                    </button>
                </Link>
            </div>
        )
    }
}

const Home = () => (
  <div>
    <h1>Welcome to the Movies Website!</h1>
  </div>
)

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/movieslist' component={MoviesList}/>
      <Route path='/newmovie' component={NewMovie}/>
      <Route path='/genreslist' component={GenresList}/>
      <Route path='/actorslist' component={ActorsList}/>
    </Switch>
  </main>
)

const Topmenu = () => (
  <header>
    <nav>
      <ul>
        <li><Link to='/'>Home</Link></li>
        <li><Link to='/movieslist'>Movies List</Link></li>
        <li><Link to='/newmovie'>Add a new movie</Link></li>
        <li><Link to='/actorslist'>Actors</Link></li>
        <li><Link to='/genreslist'>Genres</Link></li>
      </ul>
    </nav>
  </header>
)

const MainApp = () => (
  <div>
    <Topmenu />
    <Main />
  </div>
)


ReactDOM.render((
  <HashRouter>
    <MainApp />
  </HashRouter>
), document.getElementById('app'))
