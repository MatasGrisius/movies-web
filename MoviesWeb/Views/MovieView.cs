﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesWeb.Models;

namespace MoviesWeb.Views
{
    public class MovieView
    {
        public int MovieId { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public ICollection<string> Genres { get; set; }
        public ICollection<string> Actors { get; set; }

        public MovieView()
        {
            Genres = new List<string>();
            Actors = new List<string>();
        }
    }
}
