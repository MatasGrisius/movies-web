﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MoviesWeb.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<MovieGenre> MovieGenres { get; set; }
        public DbSet<MovieActor> MovieActors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=MATTDIAMONDPC;Database=MoviesDB;Trusted_Connection=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MovieGenre>()
                .HasKey(ug => new { ug.MovieId, UserId = ug.GenreId});

            modelBuilder.Entity<MovieActor>()
                .HasKey(ug => new { ug.MovieId, UserId = ug.ActorId });


            modelBuilder.Entity<Movie>()
                .HasMany(u => u.MovieGenres)
                .WithOne(ug => ug.Movie)
                .HasForeignKey(ug => ug.MovieId);

            modelBuilder.Entity<Movie>()
                .HasMany(u => u.MovieActors)
                .WithOne(ug => ug.Movie)
                .HasForeignKey(ug => ug.MovieId);
        }
    }
}
