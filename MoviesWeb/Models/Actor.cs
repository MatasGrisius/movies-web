﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesWeb.Models
{
    public class Actor
    {
        public int ActorId { get; set; }
        public string FullName { get; set; }
    }
}
